# lnmp-docker

Slogan: 一個基於docker快速搭建的lnmp環境。

**當然! 您也可以隨意修改!**

# 基於以下image:
1. https://hub.docker.com/_/mysql
2. https://hub.docker.com/_/alpine
3. https://hub.docker.com/_/nginx
4. https://hub.docker.com/_/php
5. https://hub.docker.com/_/adminer

# 適用環境(經測試後...)
1. Ubuntu 18.04 tls
2. Windows 10
3. Alpine Linux 3.10.3

# 使用方式
1. 請先安裝docker、docker-compose且clone這個儲存庫

    `$ git clone https://gitlab.com/changken/lnmp-docker.git`

2. 請先設定mysql root 密碼:

    `$ cp mysql.env.sample mysql.env`

    `$ vi mysql.env`

3. 啟動: `$ composer-compose up -d`

4. 關閉+刪除: `$ composer-compose down`

# 資料夾說明
1. nginx 放置跟nginx相關的檔案
    * html 網站根目錄 所有網頁都放在這邊
    * config nginx設定檔
        * nginx.conf nginx主設定檔
        * conf.d 網站設定檔
    * src Dockerfile預設要放入的網頁(可忽略)
2. php-fpm 放置跟php-fpm有關檔案
    * config php-fpm設定檔
        * php php.ini的所在地
            * conf.d 其他還需要引入的php動態程式庫 e.g. mysqli pdo_mysql ...
3. mysql 放置跟mysql有關的檔案
    * config mysql的設定檔
        * conf.d mysql的額外設定檔 
    * db mysql資料庫檔案位置

# ports
1. 80, 443 nginx
2. 8080 adminer
3. 3306 mysql
4. 9000 php-fpm

# 一些建議
## 請記得將權限設定正確

否則會有慘痛教訓, aka Permission denied!

`$ docker exec -it php7.3-fpm sh`

`/var/www/html # chown -R www-data:www-data wordpress(你的檔案or資料夾) `

同一時間你會在nginx-alpine容器中看到以下狀況

`$ docker exec -it nginx-alpine sh`

`/usr/share/nginx/html # ls -al`
> drwxr-xr-x    5 82       www-data      4096 Nov 23 08:27 wordpress

你一定會很疑惑為什麼在nginx-alpine中的所有者是*82*？

Ans:  因為在nginx容器中沒有一位uid為82且名子叫www-data使用者，但這個不影響使用，敬請放心。
因為nginx屬於www-data group的成員
## wordpress相關
請參考這篇文章來設定nginx

https://www.nginx.com/resources/wiki/start/topics/recipes/wordpress/

# 總而言之
docker、docker-compose真的能夠減輕搭建環境及維護的痛苦(心酸誰知)，因為只要設定一次即可處處執行！94爽啦!XD

good luck!

# 聯繫我?
Email: admin [at] changken.org

[at]請更換成@